from pathlib import Path
from utils import *
import os, sys
from sklearn.feature_extraction.text import TfidfVectorizer

# Arg 1 = PREPROCESSED DATA FOLDER
INPUT_PATH = sys.argv[1] if ( len(sys.argv) >= 2 and sys.argv[1] ) else "PREPROCESSED/"
# Arg 2 = RAKE_OUTPUT + SCORE RESULTS FOLDER
OUTPUT_PATH = sys.argv[2] if ( len(sys.argv) >= 3  and sys.argv[2] ) else "RESULTS/"
PARSED_KEYWORD = sys.argv[1] + "keywords.txt" if ( len(sys.argv) >= 2  and sys.argv[1] ) else "PREPROCESSED/keywords.txt"


nb_max_feature_per_doc = 40 # Nombre de keyword extrait par document
# Permet de réduire grandement la sortie comparé à notre systeme rake
# Plus se nombre est petit plus l'overflow sera minimisé

# 1 = Unigrammes, 2 Bigrammes, ... 
max_gram = 1 # Unigrammes donnent les meilleurs résultats
# Default only 1 class of n-grams
min_gram = max_gram
# Size Vocabulary per doc 
size_voc = 100

# Stopwords Setup :
with open('./fonctionnels_en.txt', 'r', encoding='utf-8') as f:
    lines = [line.rstrip('\n') for line in f.readlines() if not line.startswith('#')]
f.close()

# Runs sklearn TFIDF based extraction + eval   
# https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html
if __name__ == '__main__':
    corpus = []
    scores, overflows, accuracies = [], [], []
    # Function in utils.py
    pk = buildParsedKeywords(PARSED_KEYWORD)
    filenames=[]
    for p in Path(INPUT_PATH).glob('*.txt'):
        if ( not "keywords.txt" in p.name):
            with p.open() as f:
                corpus.append(" ".join(f.readlines()))
                filenames.append(p.name)
            f.close()
    vectorizer = TfidfVectorizer(
        smooth_idf = True,
        use_idf = True,
        ngram_range= (min_gram, max_gram), # Extraction par uni ou n_gram
        max_df = .9, # Default 1
        min_df = 1/len(corpus),
        max_features = size_voc * len(corpus), # Taille du modèle de langage 
        stop_words = lines
    )


    X = vectorizer.fit_transform(corpus)

    for doc in range(len(corpus)):
        # Récuperation des index des features
        feature_index = X[doc,:].nonzero()[1]
        # Récuperation des score et concaténation avec l'index des features
        tfidf_scores = list(zip(feature_index, [X[doc, x] for x in feature_index]))
        # Trier par TF.IDF MAX
        tfidf_scores.sort( key=lambda x: x[1], reverse=True)
        feature_names = vectorizer.get_feature_names()
        print("\n\nResults TF.IDF DOC n°" + str(doc))
        with open(OUTPUT_PATH + "TFIDF/" + filenames[doc], 'w') as file:
            found = ""
            # Ne prend en compte que les x feature maximisant TF.IDF
            for w, s in [(feature_names[i], s) for (i, s) in tfidf_scores[:nb_max_feature_per_doc]]:
                file.write(str(w) + " " + str(s) + "\n")
                found += w+" "
            # Function in utils.py
            score, overflow, accuracy = eval( pk[filenames[doc]], found, quiet=False )
            scores.append(score)
            overflows.append(overflow)
            accuracies.append(accuracy)
            strScore, strAccuracy, strOverflow =  "Score = " + str(score)+"% ","Accuracy = " + str(accuracy) + "% ","Overflow ratio = " + str(overflow) + " times " 
            file.write( strScore +"\n"+ strAccuracy +"\n"+ strOverflow  )
            print( ANSI.background(47) + ANSI.color_text(31) + ANSI.style_text(1))
            print( strScore +"\n"+ strAccuracy + "\n" + strOverflow, end="" )
            print( ANSI.background(1) + ANSI.color_text(0) + ANSI.style_text(0) + "\n")
        file.close()
    # Function in utils.py
    statisticsToFile(OUTPUT_PATH+"/statistics.txt", scores, accuracies, overflows, "TFIDF")
