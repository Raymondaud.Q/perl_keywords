# Setup with install.sh script 

* chmod +x install.sh
* ./install.sh

# Manual Setup

* apt install perl
* apt install libshell-perl
* apt install cpanminus
* sudo cpanm String::Util
* Create venv : `python3 -m venv rakenv`
* Enable venv : `source rakenv/bin/activate`
* pip3 install rake-nltk numpy sklearn
* chmod +x preprocess-rake.pl
* Disable venv : `deactivate`

# Launch with run.sh
* chmod +x run.sh 

## Data in DATA/
* Put your data in ./DATA/
* ./run.sh

## Specify datapath

* **OUR BEST SYSTEM WITH TF.IDF**
	* ./run.sh <DATASET_PATH>/ <PREPROCESSING_OUTPUT_PATH>/ <RESULTS_PATH>/
* **For better RAKE results**
  	* ./run.sh <DATASET_PATH>/ <PREPROCESSING_OUTPUT_PATH>/ <RESULTS_PATH>/ --remove-short-words

# Manual launch

**----/!\\----**  
<DATASET_PATH>/ <PREPROCESSING_OUTPUT_PATH>/ and <RESULTS_PATH>/ **set at `DATA/` `PREPROCESSED/` and `RESULTS/` by default**  
**----/!\\----** 

* source rakenv/bin/activate
* **OUR BEST SYSTEM WITH TF.IDF**
	* ./preprocess-rake.pl <DATASET_PATH>/ <PREPROCESSING_OUTPUT_PATH>/
* **For better RAKE results**
	* ./preprocess-rake.pl <DATASET_PATH>/ <PREPROCESSING_OUTPUT_PATH>/ --remove-short-words
* python3 pyrake.py <PREPROCESSING_OUTPUT_PATH>/  <RESULTS_PATH>/
* python3 pytfidf.py <PREPROCESSING_OUTPUT_PATH>/  <RESULTS_PATH>/
* deactivate

## Results

* **Preprocessing** : PREPROCESSED/
* **RAKE/ + TFID/ + statistics.txt** : RESULTS/
