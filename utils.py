import re, os
import numpy as np

# CLI display util class 
class ANSI():
    def background(code):
        return "\33[{code}m".format(code=code)
  
    def style_text(code):
        return "\33[{code}m".format(code=code)
  
    def color_text(code):
        return "\33[{code}m".format(code=code)

# Builds a dictionnary according to keywords.txt file
def buildParsedKeywords(PARSED_KEYWORD) :
    pk = {}
    pkf = open(PARSED_KEYWORD, 'r') 
    for line in pkf : 
        line = line.replace('\n','')
        line = line.split('>')
        parsedK =line[1]
        parsedK = re.split(';| ',parsedK)
        parsedK = list(filter(None, parsedK))
        pk[os.path.basename(line[0])] = parsedK
    pkf.close()    
    return pk

# Eval Function : Compare extracted vs parsed keywords and generate metrics :
# Overflow is the ratio between the number of keywords extracted and the number of keywords parsed
# Score mesures how many good keywords have been extracted in percent 
def eval(parsed_keywords, keywords_found, quiet=False):
    if (not quiet):
        print ('\nParsed: ', parsed_keywords)
        print ('Keywords: ', keywords_found)
    keywords_found = keywords_found.replace('\n',' ')
    keywords_found = keywords_found.split(' ')
    keywords_found = list(filter(None, keywords_found))

    cnt = 0 
    for pk in parsed_keywords :
        if ( pk in keywords_found ):
            cnt += 1
    score = cnt/len(parsed_keywords)*100
    overflow = len(keywords_found)/len(parsed_keywords)
    accuracy = cnt/len(keywords_found)*100
    return score, overflow, accuracy

def statisticsToFile(RESULT_PATH, scores, accuracies, overflows, strLabel, quiet=False):
    strScoreMean = "Score mean = " + str(np.mean(np.asarray(scores))) + "% "
    strAccuracyMean = "Accuracy mean = " + str(np.mean(np.asarray(accuracies))) + "% "
    strOverflowMean = "Overflow mean = " + str(np.mean(np.asarray(overflows))) + " times"
    if ( not quiet ):
        print( ANSI.background(43) + ANSI.color_text(31) + ANSI.style_text(1))
        print( strScoreMean + "\n" + strAccuracyMean +"\n"+ strOverflowMean, end="" )
        print( ANSI.background(1) + ANSI.color_text(0) + ANSI.style_text(0))
    with open(RESULT_PATH, 'a') as file:
        file.write( "\n\nMETHOD : " + strLabel + "\n" + strScoreMean +"\n"+ strAccuracyMean +"\n"+ strOverflowMean  )
    file.close()
