from rake_nltk import Rake
from pathlib import Path
from nltk.corpus import stopwords
from utils import *
import sys, nltk, os

nltk.download('stopwords')
nltk.download('punkt')

# Arg 1 = PREPROCESSED DATA FOLDER
INPUT_PATH = sys.argv[1] if ( len(sys.argv) >= 2 and sys.argv[1] ) else "PREPROCESSED/"
# Arg 2 = RAKE_OUTPUT + SCORE RESULTS FOLDER
OUTPUT_PATH = sys.argv[2] if ( len(sys.argv) >= 3  and sys.argv[2] ) else "RESULTS/"
PARSED_KEYWORD = sys.argv[1] + "keywords.txt" if ( len(sys.argv) >= 2  and sys.argv[1] ) else "PREPROCESSED/keywords.txt"

# Stopwords Setup :
with open('./fonctionnels_en.txt', 'r', encoding='utf-8') as f:
    lines = [line.rstrip('\n') for line in f.readlines() if not line.startswith('#')]
stop_words = set(stopwords.words('english'))
stop_words.update(lines)

# Runs RAKE + eval    
if __name__ == '__main__':
    scores, overflows, accuracies = [], [], []
    # Function in utils.py
    pk = buildParsedKeywords(PARSED_KEYWORD)
    for p in Path(INPUT_PATH).glob('*.txt'):
        if ( not "keywords.txt" in p.name):
            with p.open() as f:
                lines = f.readlines()
            f.close()

            # Uses stopwords for english from NLTK, and all puntuation characters by default
            r = Rake(
                # max_length = 3,
                # min_length = 3,
                include_repeated_phrases = False, # Ignore repeated phrases. Most likely clutter.
                # language='english'
                stopwords = stop_words
            )

            # Extraction given the list of strings where each string is a sentence.
            r.extract_keywords_from_sentences(lines)

            # To get keyword phrases ranked highest to lowest with scores.
            with open(OUTPUT_PATH + "RAKE/" + p.name, 'w') as file:
                print("INPUT : " + INPUT_PATH+p.name + "\nResults : "+OUTPUT_PATH+p.name, end="")
                # Reference keyword parsed during preprocessing of current file
                res = r.get_ranked_phrases_with_scores()[:10]
                found = ""
                for elem in res:
                    file.write(str(elem)+"\n")
                    found += elem[1] + "\n"
                # Function in utils.py
                score, overflow, accuracy = eval( pk[p.name], found, quiet=True ) 
                scores.append(score)
                overflows.append(overflow)
                accuracies.append(accuracy)
                strScore, strAccuracy, strOverflow =  "Score = " + str(score)+"% ","Accuracy = " + str(accuracy) + "% ","Overflow ratio = " + str(overflow) + " times " 
                file.write( strScore +"\n"+ strAccuracy +"\n"+ strOverflow  )
                print( ANSI.background(47) + ANSI.color_text(31) + ANSI.style_text(1))
                print( strScore +"\n"+ strAccuracy + "\n" + strOverflow, end="" )
                print( ANSI.background(1) + ANSI.color_text(0) + ANSI.style_text(0) + "\n")
            file.close()
    # Function in utils.py
    statisticsToFile(OUTPUT_PATH+"/statistics.txt", scores, accuracies, overflows, "RAKE", quiet=False)