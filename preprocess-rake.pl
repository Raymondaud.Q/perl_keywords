#!/usr/bin/perl 

# Installing CPAN & CPANM : https://www.cpan.org/modules/INSTALL.html
# cpan App::cpanminus
# cpanm Simple
# cpanm String::Util
# Strawberry Perl's MINGW needs to be primary location in system environment variables.
# Need to uninstall Perl before installing a new version over it.


# Modules used 
use strict; 
use warnings; 
use English;
use String::Util qw(ltrim);

my ($path_in_data, $path_out_preprocessing, $remove_short_words) = @ARGV;

# Arg 1 = DATA FOLDER
if (not defined $path_in_data) {
    $path_in_data = "DATA";
}

# Arg 2 = PREPROCESSING OUTPUT FOLDER
if (not defined $path_out_preprocessing) {
    $path_out_preprocessing = "PREPROCESSED";
}
my $path_out_keywords = $ARGV[1] ? "$ARGV[1]/keywords.txt" : "$path_out_preprocessing/keywords.txt";

# Arg 3 = REMOVE SHORT WORDS
if (defined $remove_short_words and $remove_short_words ne '--remove-short-words') {
    # If the 3rd parameter isn't '--remove-short-words' then undefine the
    # variable.
    undef $remove_short_words;
}

# Regular Expressions
my $regex_delete_line = "^(TYPE|DOI|DATE|AUTHOR|ADDRESS|JOURNAL|\_\_SECTION\_\_)"; #JOURNAL
my $regex_delete_token = "^(TITLE|ACRONYMS|ABSTRACT|KEYWORDS)(\t|\ )+"; #JOURNAL
my $regex_keywords = "^(KEYWORDS)";
my $regex_tex = qr/(\\\((.*?)\\\)|<\ *tex-math(.*?)\/\ *tex-math\ *>|<mml:math(.*?)\/math>)/;
my $regex_pacs_numbers = qr/[0-9]{2}\.[0-9]{2}\..{2}/;
my $regex_keep_chars = qr/[^a-zA-Z\ \n\-\;]/; # Tried keeping periods and apostrophes: \.\'
my $regex_start_end_dashes = qr/\ \K\-+(?=[a-zA-Z]+)|[a-zA-Z]*\K\-+(?=\ +)/;
my $regex_small_words = qr/(\ +)\K[a-zA-Z\-]{1,3}(?=\;|\ |\n+)/;
my $regex_multi_space = qr/\ {2,}/;

truncate  $path_out_keywords, 0;
my $keywords = "";

my $dir = "./$path_in_data";
foreach my $fp (glob("$dir/*.txt")) {
    # if ($fp ne "$dir/jpco_1_1_011001.txt") {
    #     last;
    # }

    # https://stackoverflow.com/questions/392643/how-can-i-use-a-variable-in-the-replacement-side-of-the-perl-substitution-operat
    (my $outfile = $fp) =~ s/$path_in_data/'"$path_out_preprocessing"'/ee;
    printf "In File : %s\n", $fp;
    printf "Out File: %s\n", $outfile;

    open my $fhr, "<", $fp or die "can't read open '$fp': $OS_ERROR";
    open(my $fhw, '>', $outfile) or die "Could not open file '$outfile' $!";
    open(my $keyresults, '>>', $path_out_keywords ) or die "Could not open file '$path_out_keywords' $!";
    while (<$fhr>) {
        # Convert line to lowercase and remove section/label tokens.
        (my $line = lc $_) =~ s/$regex_delete_token//i;

        # Only print the keywords to the screen.
        if ($_ =~ /$regex_keywords/)
        {
            # Remove PACS numbers from keywords.
            # https://ufn.ru/en/pacs/
            $line =~ s/$regex_pacs_numbers//g;

            # Only keep alpha characters, spaces, new lines (\n), dashes and semicolons.
            # https://stackoverflow.com/questions/31625634/perl-regex-remove-all-characters-except-alphanumeric-characters-and-comma/31626549
            $line =~ s/$regex_keep_chars//g;

            printf "KEYWORDS     : %s", $line;
            printf $keyresults "%s> %s", $fp, $line
        }
        # Only print the ignored lines to the screen.
        elsif ($_ =~ /$regex_delete_line/)
        {
            printf "Removed line : %s", $_;
        }
        else
        {
            # Remove math formulas.
            $line =~ s/$regex_tex//g;

            # Replace apostrophes and underscores with a space.
            # $line =~ s/[\'\_]/\ /g;
            # $line =~ s/[\_]/\ /g;

            # Only keep alpha characters, spaces, new lines (\n), dashes and semicolons.
            # https://stackoverflow.com/questions/31625634/perl-regex-remove-all-characters-except-alphanumeric-characters-and-comma/31626549
            $line =~ s/$regex_keep_chars//g;

            # Remove leading/trailing dashes.
            $line =~ s/$regex_start_end_dashes//g;

            # Remove one and two character "words".
            # https://stackoverflow.com/questions/68035102/perl-regular-expression-for-string-and-digits-match-only-last-6-digits
            if (defined $remove_short_words)
            {
                $line =~ s/$regex_small_words//g;
            }

            # Compress multiple spaces and tabs down to a single space.
            $line =~ s/$regex_multi_space/ /g;

            # Trim leading spaces from the line.
            $line = ltrim($line);

            # Print non-empty lines to the preprocessed output file.
            if ($line ne "")
            {
                # Remove semicolons.
                $line =~ tr/\;//d;

                printf $fhw "%s", $line;
            }
        }
    }
    print "\n";

    close $fhr or die "can't read close '$fp': $OS_ERROR";
    close $fhw or die "can't read close '$outfile': $OS_ERROR";
    close $keyresults or die "can't read close '$path_out_keywords': $OS_ERROR";
}