
[[ -z $2 ]] && rm -f ./PREPROCESSED/* || rm -f $2/*
source rakenv/bin/activate 
./preprocess-rake.pl $1 $2 $4
python3 pyrake.py $2 $3
python3 pytfidf.py $2 $3
deactivate
[[ -z $1 ]] && echo "No output ? -usage: ./run.sh <DATASET_PATH>/ <PREPROCESSING_OUTPUT_PATH>/ <RESULTS_PATH>/   "